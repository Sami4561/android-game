package com.example.plane.model;

import android.graphics.Canvas;
import android.graphics.Point;

import java.util.ArrayList;

public class ObstacleManager {

    private ArrayList<Obstacle> lesObstacles;
    private int espaceJ;
    private int espaceObs;
    private int obsHeight;
    private int color;

    private long debutT;

    public ObstacleManager(int espaceJ,int espaceObs, int obsHeight,int color){
        this.espaceJ = espaceJ;
        this.espaceObs = espaceObs;
        this.obsHeight = obsHeight;
        this.color = color;

        debutT = System.currentTimeMillis();

        lesObstacles = new ArrayList<>();

        creerlesObstacles();
    }

    private void creerlesObstacles(){
        int y = -5*Constante.SCREEN_HEIGHT/4;
        while (y<0){
            int xStart = (int)(Math.random()*(Constante.SCREEN_WIDTH-espaceJ));
            lesObstacles.add(new Obstacle(obsHeight,color,xStart,y,espaceJ));
            y+=obsHeight+espaceObs;
        }
    }

    public boolean colisionJoueur(RecJoueur joueur){
        for (Obstacle obs : lesObstacles){
            if (obs.playerCollide(joueur)){
                return true;
            }
        }
        return false;
    }

    public int obsFranchis(Point joueur){
        int nbFranchis = 0;
        for (Obstacle obs : lesObstacles){
            if (obs.estFranchis(joueur)){
                nbFranchis++;
            }
        }
        return nbFranchis;
    }

    public void update() {
        int tempsEcoule = (int)(System.currentTimeMillis() - debutT);
        debutT = System.currentTimeMillis();
        for (Obstacle obs : lesObstacles){
          obs.descendre(Constante.speed*tempsEcoule);
        }
        if (lesObstacles.get(lesObstacles.size()-1).getRectangle1().top>=Constante.SCREEN_HEIGHT){
            int xStart= (int)(Math.random()*(Constante.SCREEN_WIDTH - espaceJ));
            lesObstacles.add(0,new Obstacle(obsHeight,color,xStart,lesObstacles.get(0).getRectangle1().top+ obsHeight - espaceObs,espaceJ));
            lesObstacles.remove(lesObstacles.size()-1);
        }
    }

    public void draw(Canvas canvas){
        for (Obstacle obs : lesObstacles){
            obs.draw(canvas);
        }
    }




}
