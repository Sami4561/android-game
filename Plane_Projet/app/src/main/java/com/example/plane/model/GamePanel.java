package com.example.plane.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.view.View;

import com.example.plane.R;

import java.util.PrimitiveIterator;

public class GamePanel extends View {

    private boolean debut = true;
    private RecJoueur joueur;
    private Point joueurPoint;
    private Rect r = new Rect();
    private ObstacleManager obstacleManager;
    private boolean enMouvement = true;
    private boolean finDePartie = false;
    private long tempsFinJeu;
    private Context context;

    private BitmapFactory bf;
    private Bitmap plane;

    public GamePanel(Context context){
        super(context);

        joueur = new RecJoueur(new Rect(100,75,200,150), Color.TRANSPARENT);
        joueurPoint = new Point(Constante.SCREEN_WIDTH/2,Constante.SCREEN_HEIGHT*5/6);
        joueur.update(joueurPoint);

        this.context = context;

        obstacleManager = new ObstacleManager(300,700,75,Color.rgb(95,0,215));

        bf = new BitmapFactory();
        plane = bf.decodeResource(context.getResources(), R.drawable.centerplaneblack);

        setFocusable(true);

    }

    public void update(){
        if (!finDePartie && !debut){
            joueur.update(joueurPoint);
            obstacleManager.update();
            invalidate();
            int franchis = obstacleManager.obsFranchis(joueurPoint);
            if (franchis != 0){
                Constante.score += franchis;
                if (Constante.score%10 == 0 && Constante.score<100){
                    Constante.speed = (float)(Constante.speed*1.10);
                }
            }

            Constante.score += obstacleManager.obsFranchis(joueurPoint);

            if (obstacleManager.colisionJoueur(joueur)){
                finDePartie = true;
                tempsFinJeu = System.currentTimeMillis();
                if(Constante.score == 111) {
                    Constante.nbVictoires += 1;
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (finDePartie){
            Constante.score = -3;
            Constante.speed = Constante.SCREEN_HEIGHT/2500.0f;
            reset();
        }
        if (debut){
            Constante.score= -3;
            debut = false;
            reset();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        Paint paint = new Paint();
        paint.setTextSize(100);
        paint.setColor(Color.WHITE);


        joueur.draw(canvas);
        obstacleManager.draw(canvas);

        canvas.drawBitmap(plane,null,joueur.getRectangle(),new Paint());

        if (finDePartie){
            if (Constante.score == 111){
                drawTextBas(canvas, paint, "You Win!");
            }
            drawTextCentre(canvas,paint,"Score: " + Constante.score);
        }
        if (debut){
            drawTextCentre(canvas,paint,"Tap To Play");
            drawTextBas(canvas, paint, "Wins: " + Constante.nbVictoires);
        }
    }


    public void drawTextCentre(Canvas canvas, Paint paint,String texte){
        paint.setTextAlign(Paint.Align.LEFT);

        canvas.getClipBounds(r);

        int cHeight = r.height();
        int cWeight = r.width();

        paint.getTextBounds(texte,0,texte.length(),r);
        float x = cWeight/2f - r.width()/2f - r.left;
        float y = cHeight/2f - r.height()/2f - r.bottom;
        canvas.drawText(texte,x,y,paint);
    }

    public void drawTextBas(Canvas canvas, Paint paint,String texte){
        paint.setTextAlign(Paint.Align.LEFT);

        canvas.getClipBounds(r);

        int cHeight = r.height();
        int cWeight = r.width();

        paint.getTextBounds(texte,0,texte.length(),r);
        float x = cWeight/2f - r.width()/2f - r.left;
        float y = cHeight - r.height()/2f- r.bottom;
        canvas.drawText(texte,x,y,paint);
    }

    public  void reset(){
        joueurPoint = new Point(Constante.SCREEN_WIDTH/2,Constante.SCREEN_HEIGHT*5/6);
        joueur.update(joueurPoint);
        obstacleManager = new ObstacleManager(700,1000,75, Color.rgb(95,0,215));
        enMouvement = true;
        finDePartie = false;

    }

    public void deplaceJ(int x){
        if (!finDePartie && enMouvement && peutBouger(x)){
            if (x>=1){
                plane = bf.decodeResource(context.getResources(),R.drawable.rigthplaneblack);
            }
            if (x<=-1){
                plane = bf.decodeResource(context.getResources(),R.drawable.leftplaneblack);
            }
            if (x>-1 && x<1){
                plane = bf.decodeResource(context.getResources(),R.drawable.centerplaneblack);
            }
            joueurPoint.set(joueurPoint.x + x,joueurPoint.y);
        }
    }

    public boolean peutBouger(int x){
        if (joueurPoint.x + x <= joueur.getRectangle().width()/2-1 || joueurPoint.x+x>=Constante.SCREEN_WIDTH-joueur.getRectangle().width()/2+1){
            return false;
        }
        return true;
    }



}
