package com.example.plane.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class RecJoueur implements GameObjet{

    private Rect rectangle;
    private int couleur;

    public RecJoueur(Rect rectangle,int couleur){
        this.rectangle = rectangle;
        this.couleur = couleur;
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(couleur);
        canvas.drawRect(rectangle,paint);
    }

    @Override
    public void update() {

    }

    public  void update(Point point){
        rectangle.set(point.x-rectangle.width()/2,point.y-rectangle.height()/2,point.x+rectangle.width()/2,point.y+rectangle.height()/2);
    }

    public Rect getRectangle(){
        return rectangle;
    }
}
