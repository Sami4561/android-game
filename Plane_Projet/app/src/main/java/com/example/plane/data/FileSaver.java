package com.example.plane.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.plane.model.Constante;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class FileSaver implements Sauveur {
    @Nullable
    @Override
    public void save(@NonNull OutputStream fichier) {
        try (ObjectOutputStream oos = new ObjectOutputStream(fichier)) {
            oos.writeInt(Constante.nbVictoires);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
