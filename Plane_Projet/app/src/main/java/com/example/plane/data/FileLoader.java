package com.example.plane.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.plane.model.Constante;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class FileLoader implements Loader {
    @Nullable
    @Override
    public int load(@NonNull InputStream fichier) {
        int retour = 0;
        try (ObjectInputStream ois = new ObjectInputStream(fichier)) {
            retour = ois.readInt();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retour;
    }
}
