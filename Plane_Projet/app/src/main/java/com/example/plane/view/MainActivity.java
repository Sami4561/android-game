package com.example.plane.view;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.plane.R;
import com.example.plane.data.FileLoader;
import com.example.plane.data.FileSaver;
import com.example.plane.data.Stub;
import com.example.plane.loop.MainThread;
import com.example.plane.model.Constante;
import com.example.plane.model.GamePanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends Activity {

    public static final String VICTOIRES = "Victoires";
    private GamePanel gamePanel;
    private MainThread mainThread;
    private int xPos = 0;
    private RelativeLayout relativeLayout;
    private FileLoader fileLoader = new FileLoader();
    private FileSaver fileSaver = new FileSaver();
    private Stub stub = new Stub();

    //récupérer les capteurs
    private SensorManager sensorManager;

    //gyroscope
    private Sensor gyroscopeSensor;

    private SensorEventListener gyroscopeEventListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        //récupérer les dimensions de l'écran
        Constante.SCREEN_HEIGHT = displayMetrics.heightPixels;
        Constante.SCREEN_WIDTH = displayMetrics.widthPixels;
        Constante.speed = Constante.SCREEN_HEIGHT/2500.0f;

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        //Chargement nombre de victoires
        try {
            Constante.nbVictoires = fileLoader.load(openFileInput(VICTOIRES));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(Constante.nbVictoires < 0) {
            try {
                Constante.nbVictoires = stub.load(openFileInput(VICTOIRES));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (gyroscopeSensor == null){
            Toast.makeText(this,"Problème accéléromètre.",Toast.LENGTH_SHORT).show();
            finish();
        }



        gyroscopeEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
                    xPos = -(int)event.values[0]*1;
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };


        this.gamePanel = new GamePanel(this);

        setContentView(R.layout.main_activity);

        relativeLayout = (RelativeLayout)findViewById(R.id.relative);

        relativeLayout.addView(gamePanel);

        mainThread = new MainThread(this);
        mainThread.setRunning(true);
        mainThread.start();

    }

    public  void update(){
        gamePanel.deplaceJ(xPos);
        gamePanel.update();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(gyroscopeEventListener,gyroscopeSensor,SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(gyroscopeEventListener);
    }

    @Override
    protected void onDestroy() {
        //Enregistrement nombre de victoires
        try {
            fileSaver.save(openFileOutput(VICTOIRES, MODE_PRIVATE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
