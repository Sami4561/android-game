package com.example.plane.loop;

import android.graphics.Canvas;

import com.example.plane.view.MainActivity;

public class MainThread extends Thread {

    public  static final int MAX_FPS = 90;
    private double averageFPS;
    private MainActivity mainActivity;
    private boolean running;
    public static Canvas canvas;

    public MainThread(MainActivity mainActivity){
        super();
        this.mainActivity = mainActivity;
    }

    public void setRunning(boolean running){
        this.running = running;
    }

    @Override
    public void run() {
        long startT;
        long timeMillis = 1000/MAX_FPS;
        long waitT;
        int frameCount = 0;
        long totalTime = 0;
        long targetTime = 1000/MAX_FPS;

        while (running){
            startT = System.nanoTime();
            canvas = null;
            try{
                this.mainActivity.update();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                if (canvas !=null){
                    try{

                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
            timeMillis = (System.nanoTime() - startT)/1000000;
            waitT = targetTime - timeMillis;
            try {
                if (waitT >0){
                    this.sleep(waitT);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            totalTime += System.nanoTime() - startT;
            frameCount++;
            if (frameCount == MAX_FPS){
                averageFPS = 1000/((totalTime/frameCount)/1000000);
                frameCount = 0;
                totalTime = 0;
                System.out.println(averageFPS);
            }


        }







    }
}
