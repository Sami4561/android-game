package com.example.plane.data;

import java.io.OutputStream;

public interface Sauveur {

    void save(OutputStream fichier);
}
