package com.example.plane.model;

import android.graphics.Canvas;

public interface GameObjet {

    public void draw(Canvas canvas);

    public void update();

}
