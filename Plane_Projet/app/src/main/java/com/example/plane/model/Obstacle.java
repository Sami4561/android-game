package com.example.plane.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class Obstacle implements GameObjet {

    private Rect rectangle1;
    private Rect rectangle2;
    private int couleur;
    private int espaceJ;
    private int startX;
    private boolean franchis = false;

    public Obstacle(int rectHeight,int couleur,int startX,int startY,int espaceJ){
        this.couleur = couleur;

        rectangle1 = new Rect(0,startY,startX,startY+rectHeight);
        rectangle2 = new Rect(startX+espaceJ,startY,Constante.SCREEN_WIDTH,startY+rectHeight);
    }

    public void descendre(float y){
        rectangle1.top+=y;
        rectangle1.bottom += y;
        rectangle2.top+=y;
        rectangle2.bottom += y;
    }

    public boolean playerCollide(RecJoueur joueur){
        return Rect.intersects(rectangle1,joueur.getRectangle())||Rect.intersects(rectangle2,joueur.getRectangle());
    }

    public boolean estFranchis(Point joueur){
        if (franchis){
            return false;
        }
        if (joueur.x>rectangle1.top){
            franchis = true;
            return true;
        }
        return false;
    }


    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(couleur);
        canvas.drawRect(rectangle1,paint);
        canvas.drawRect(rectangle2,paint);

    }

    @Override
    public void update() {

    }

    public Rect getRectangle1() {
        return rectangle1;
    }
}
