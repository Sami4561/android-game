package com.example.plane.data;

import java.io.InputStream;

public interface Loader {

    int load(InputStream fichier);
}
